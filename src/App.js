import React, { useContext, useEffect } from "react";
import { Login, Home } from "./containers";
import { BrowserRouter, Route } from "react-router-dom";
import { StateProvider } from "./context";
import StockProvider from './context/stockContext';
// import { useHistory } from "react-router-dom";
// import { StateContext } from "./context"

function App() {
  // const history = useHistory();

  return (
      // initialState={{ users: ["Oli", "seba"] }}> */}
    <StateProvider>
      <StockProvider>
        <BrowserRouter>
          <Route exact path="/" component={Login} />
          <Route exact path="/home" component={Home} />
        </BrowserRouter>
      </StockProvider>
    </StateProvider>
  );
}

export default App;
