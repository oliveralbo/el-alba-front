class Functions {
  processToRecipes = (process, rawMaterials, products) => {
    let productKey = Object.keys(process.pro)
    let pro
    let ingredientsKey = Object.keys(process.mp)
    let ingredients = []
    let valuesRm = []
    for (let i = 0; i <= ingredientsKey.length; i++) {
      rawMaterials.forEach((rm) => {
        //ver aca si va un number con el objeto posta
        if (rm._id === ingredientsKey[i]) {
          ingredients.push(rm.product)
        }
      })
    }
    products.forEach((p) => {
      if (p._id === productKey[0]) {
        pro = p.product
      }
    })

    let prodId = Object.keys(process.pro)
    let ingrsId = Object.keys(process.mp)

    let objectRecipe = {}
    objectRecipe.product = {}
    objectRecipe.product.key = pro
    objectRecipe.product.value = process.pro[prodId[0]]
    objectRecipe.ingredients = {}
    objectRecipe.ingredients.keys = ingredients
    ingrsId.forEach((rm) => {
      valuesRm.push(process.mp[rm])
    })
    objectRecipe.ingredients.values = valuesRm

    return objectRecipe
  }

  recipesToProcess = (recipe, rawMaterials, products) => {
    let proceso = {}
    proceso.mp = {}
    proceso.pro = {}
    let productKey = recipe.product.key
    let ingredientsKey = Object.values(recipe.ingredients.keys)
    for (let i = 0; i <= ingredientsKey.length; i++) {
      rawMaterials.forEach((rm) => {
        if (rm.product === ingredientsKey[i]) {
          proceso.mp[rm._id] = recipe.ingredients.values[i]
        }
      })
    }
    products.forEach((p) => {
      if (p.product === productKey) {
        proceso.pro[p._id] = recipe.product.value
      }
    })

    return proceso
  }

  removeItemFromArr = (arr, item) => {
    var i = arr.indexOf(item)

    if (i !== -1) {
      arr.splice(i, 1)
    }
  }
}
const gralFunctions = new Functions()

export default gralFunctions
