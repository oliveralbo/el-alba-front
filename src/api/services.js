/* eslint-disable import/no-anonymous-default-export */
// import stockMP from '../mock/stockMP'
// import stockProd from '../mock/stockProd'
// import stockProd2 from '../mock/stockProd2'
// import clients from '../mock/clients'
import providers from '../mock/providers'
// import recipes from '../mock/recipes'
// import allUsers from '../mock/allUsers'
const bearer_token = localStorage.getItem('usrToken')

const getUsers = async () => {
  let url = 'http://localhost:5000/usuarios'
  let users = await fetch(url, {
    method: 'GET',
    withCredentials: true,
    headers: {
      token: bearer_token,
      'Content-Type': 'application/json',
    },
  })
    .then((response) => response.json())
    .then((data) => (data, { data }))
    .catch(function (error) {
      console.log('Request failed', error)
    })

  return users.data
  // return allUsers;
}

const addUsers = async (data) => {
  return await fetch('http://localhost:5000/usuario/', {
    method: 'post',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then(function (response) {
      return response.json()
    })
    .then((usuario) => {
      return usuario
    })
  // return addUsers;
}

const login = async (data) => {
  return await fetch('http://localhost:5000/login', {
    method: 'POST',
    // mode: "cors",
    body: JSON.stringify(data),
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
    },
  })
    .then(function (response) {
      return response.json()
    })
    .then((user) => {
      let errors = user.err
      if (errors) throw new Error(errors.message)
      return user
    })
  //return JSON.stringify(user, 0, 2);
}

const deleteUser = async (id) => {
  let url = `http://localhost:5000/usuario/${id}`
  let users = await fetch(url, {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then((response) => response.json())
    .then((data) => (data, { data }))
    .catch(function (error) {
      console.log('Request failed', error)
    })

  return users.data
  // return allUsers;
}

const deleteRawMaterial = async (id) => {
  let url = `http://localhost:5000/materiaPrima/${id}`
  let users = await fetch(url, {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then((response) => response.json())
    .then((data) => (data, { data }))
    .catch(function (error) {
      console.log('Request failed', error)
    })

  return users.data
  // return allUsers;
}

const deleteProduct = async (id) => {
  let url = `http://localhost:5000/producto/${id}`
  let users = await fetch(url, {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then((response) => response.json())
    .then((data) => (data, { data }))
    .catch(function (error) {
      console.log('Request failed', error)
    })

  return users.data
  // return allUsers;
}

const deleteOrders = async (id) => {
  let url = `http://localhost:5000/pedido/${id}`
  let users = await fetch(url, {
    method: 'DELETE',
    withCredentials: true,
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then((response) => response.json())
    .then((data) => (data, { data }))
    .catch(function (error) {
      console.log('Request failed', error)
    })

  return users.data
  // return allUsers;
}

const deleteRecipe = async (id) => {
  let url = `http://localhost:5000/receta/${id}`
  let res = await fetch(url, {
    method: 'DELETE',
    withCredentials: true,
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then((response) => response.json())
    .then((data) => (data, { data }))
    .catch(function (error) {
      console.log('Request failed', error)
    })

  return res.data
  // return allUsers;
}

const getRawMaterial = async () => {
  return fetch('http://localhost:5000/materiasprimas', {
    method: 'GET',
    headers: new Headers({
      token: bearer_token,
      'Content-Type': 'application/json',
    }),
  })
    .then((response) => response.json())
    .then((data) => (data, { data }))
    .catch(function (error) {
      console.log('Request failed', error)
    })

  // return stockMP;
}

const getAllProductDone = async () => {
  return fetch('http://localhost:5000/productos', {
    method: 'GET',
    headers: new Headers({
      token: bearer_token,
      'Content-Type': 'application/json',
    }),
  })
    .then((response) => response.json())
    .then((data) => (data, { data }))
    .catch(function (error) {
      console.log('Request failed', error)
    })

  // return stockProd;
}

const getClients = async () => {
  return fetch('http://localhost:5000/clientes', {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json',
      token: bearer_token,
    }),
  })
    .then((response) => response.json())
    .then((data) => (data, { data }))
    .catch(function (error) {
      console.log('Request failed', error)
    })

  // let clientes = clients
  // for (let cliente of clientes) {
  //   delete cliente.payments
  //   delete cliente.orders
  // }
  // return clientes;
}

const getPayments = async (id) => {
  return fetch(`http://localhost:5000/cliente/pagos/${id}`, {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json',
      token: bearer_token,
    }),
  })
    .then((response) => response.json())
    .then((data) => (data, { data }))
    .catch(function (error) {
      console.log('Request failed', error)
    })

  // let clientes = clients
  // for (let cliente of clientes) {
  //   delete cliente.payments
  //   delete cliente.orders
  // }
  // return clientes;
}

const getClientById = async (id) => {
  return fetch(`http://localhost:5000/cliente/${id}`, {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json',
      token: bearer_token,
    }),
  })
    .then((response) => response.json())
    .then((data) => (data, { data }))
    .catch(function (error) {
      console.log('Request failed', error)
    })

  // let clientes = clients
  // for (let cliente of clientes) {
  //   delete cliente.payments
  //   delete cliente.orders
  // }
  // return clientes;
}

const getProviders = async () => {
  // return fetch("http://localhost:5000/rawMaterial/getAllClients")
  //   .then(response => response.json())
  //   .then(data => (data, { data }))
  //   .catch(function(error) {
  //     console.log("Request failed", error);
  //   });

  return providers
}

const getOrdersUndelivered = async () => {
  return fetch('http://localhost:5000/pedidos/noentregados', {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json',
      token: bearer_token,
    }),
  })
    .then((response) => response.json())
    .then((data) => (data.records, { data }))
    .catch(function (error) {
      console.log('Request failed', error)
    })

  // return ordersUndelivered;
}

const getRecipes = async () => {
  return fetch('http://localhost:5000/recetas', {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json',
      token: bearer_token,
    }),
  })
    .then((response) => response.json())
    .then((data) => (data.records, { data }))
    .catch(function (error) {
      console.log('Request failed', error)
    })

  // return recipes;
}

const addRecipes = async (data) => {
  return await fetch('http://localhost:5000/receta/', {
    method: 'post',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then((response) => response.json())
    .then((data) => {
      if (data.ok) {
        return data, { data }

        // handle form validation errors, response.data.errors...
      } else {
        // console.log("acac, " , data)
        throw new Error(data.err.message)
      }
    })
}

const addPayment = async (data) => {
  return await fetch('http://localhost:5000/cliente/pago/', {
    method: 'post',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then(function (response) {
      return response.json()
    })
    .then((receta) => {
      return receta
    })
  // return addUsers;
}

const addProduct = async (data) => {
  return await fetch('http://localhost:5000/producto', {
    method: 'post',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then(function (response) {
      return response.json()
    })
    .then((receta) => {
      return receta
    })
  // return addUsers; addProduct
}

const addRawMaterial = async (data) => {
  return await fetch('http://localhost:5000/materiaprima', {
    method: 'post',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then(function (response) {
      return response.json()
    })
    .then((receta) => {
      return receta
    })
  // return addUsers; addProduct
}

const deliveryUser = async (data, id) => {
  return await fetch(`http://localhost:5000/cliente/entrega/${id}`, {
    method: 'put',
    body: JSON.stringify({ amount: data }),
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then(function (response) {
      return response.json()
    })
    .then((receta) => {
      return receta
    })
  // return addUsers;
}

const deliveryOrder = async (data, id) => {
  return await fetch(`http://localhost:5000/pedido/${id}`, {
    method: 'put',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then(function (response) {
      return response.json()
    })
    .then((receta) => {
      return receta
    })
  // return addUsers;
}

const process = async (data) => {
  return await fetch(`http://localhost:5000/proceso`, {
    method: 'put',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then(function (response) {
      return response.json()
    })
    .then((oka) => {
      return oka
    })
  // return process;
}

const updateRawMaterial = async (id, data) => {
  return await fetch(`http://localhost:5000/materiaprima/${id}`, {
    method: 'put',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then(function (response) {
      return response.json()
    })
    .then((oka) => {
      return oka
    })
  // return process;
}

const updateUser = async (id, data) => {
  return await fetch(`http://localhost:5000/usuario/${id}`, {
    method: 'put',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then(function (response) {
      return response.json()
    })
    .then((oka) => {
      return oka
    })
  // return process;
}

const updateProduct = async (id, data) => {
  return await fetch(`http://localhost:5000/producto/${id}`, {
    method: 'put',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then(function (response) {
      return response.json()
    })
    .then((oka) => {
      return oka
    })
  // return process;
}

const saveOrder = async (data) => {
  return await fetch('http://localhost:5000/pedido/', {
    method: 'post',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      token: bearer_token,
    },
  })
    .then(function (response) {
      return response.json()
    })
    .then((receta) => {
      return receta
    })
  // return saveOrder
}

export default {
  addUsers,
  addRecipes,
  addPayment,
  addProduct,
  addRawMaterial,
  updateRawMaterial,
  updateUser,
  updateProduct,
  saveOrder,
  deleteOrders,
  deleteRawMaterial,
  deleteProduct,
  deleteUser,
  deleteRecipe,
  getUsers,
  deliveryUser,
  deliveryOrder,
  login,
  getOrdersUndelivered,
  getRawMaterial, // agregarle el all o el plural
  getAllProductDone,
  getClients,
  getPayments,
  getClientById,
  getProviders,
  getRecipes,
  process,
}
