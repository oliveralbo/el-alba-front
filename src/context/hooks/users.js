import { useSetAppState, useAppState } from "../";

export const useUsers = () => {
  const setAppState = useSetAppState();
  const { users } = useAppState();

  return {
    users,
    getUser: user => users.find(u => u === user),
    addUser: user => {
      setAppState({ users: users.concat(user) });
    },
    updateUser: () => {},
    deleteUser: () => {}
  };
};
