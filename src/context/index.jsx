import React, { useContext, useState } from "react";

const StateContext = React.createContext(null);
const FormContext = React.createContext(null);

export const StateProvider = ({ children, initialState = {} }) => {
  const [state, setState] = useState(initialState);
  const [formData, setFormData] = useState(null)

  return (
    <StateContext.Provider value={[state, setState,formData, setFormData]}>
      {children}
    </StateContext.Provider>
  );
};

export const useAppState = () => {
  const [state] = useContext(StateContext);
  return state;
};

export const useSetAppState = () => {
  const [_, setState] = useContext(StateContext);
  return setState;
};


export const useFormData = () => {
  const [formData] = useContext(StateContext);
  return formData;
};

export const useSetFormData = (data) => {
  const [_, setFormData] = useContext(StateContext);
  return setFormData;
};
