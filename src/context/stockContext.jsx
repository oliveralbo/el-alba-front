import React, { createContext, useState } from 'react'

import services from '../api/services'

const MyFunction = ({ children }) => {
  const [contextMp, setContextMp] = useState(null)
  const [contextProd, setContextProd] = useState(null)
  // const [state, setstate] = useState({})

  const getStocks = async () => {
    try {
      const mp = await services.getRawMaterial()
      const productos = await services.getAllProductDone()
      setContextMp(mp.data.records)
      setContextProd(productos.data.records)
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <stockContext.Provider
      value={{
        contextMp,
        setContextMp,
        contextProd,
        setContextProd,
        getStocks,
      }}
    >
      {children}
    </stockContext.Provider>
  )
}

export const stockContext = createContext()
export default MyFunction
