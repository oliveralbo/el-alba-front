import React, { useState } from 'react'
import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import services from '../../api/services'
import { useHistory } from 'react-router-dom'
import { useSetAppState } from '../../context'

const useStyles = makeStyles((theme) => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  userFailed: {
    marginTop: '5%',
    marginLeft: '8%',
    color: 'red',
  },
  error: {
    textAlign: 'center',
    fontSize: '20px',
    fontFamily: 'Impact Haettenschweiler Arial Narrow Bold',
  },
}))

export default function SignIn() {
  const setAppState = useSetAppState()
  const [users, setUsers] = useState(null)
  const [name, setName] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState(null)
  const classes = useStyles()
  const history = useHistory()

  const handleChange = (estado) => (event) => {
    if (estado === 'name') {
      setName(event.target.value)
    } else if (estado === 'password') {
      setPassword(event.target.value)
    }
  }

  const userLogin = async () => {
    try {
      const users = await services.login({ name, password })
      setUsers(users)
      localStorage.setItem('usrToken', users.token)
      setAppState({ userContext: users.user }) //si se pasa el user en el history esto podria sacarse
      history.push('/home', { users })
    } catch (e) {
      if (e.message === 'Failed to fetch') {
        setError('UPS!!! Algo salio mal, verique su conexion a internet...')
      } else {
        setError(e.message)
      }
    }
    setName('')
    setPassword('')
  }

  const handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      userLogin()
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Doña LiLi
        </Typography>
        <TextField
          type="text"
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="name"
          label="Nombre"
          name="name"
          autoComplete="name"
          autoFocus
          value={name}
          onChange={handleChange('name')}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="password"
          label="Password"
          type="password"
          id="password"
          value={password}
          autoComplete="password"
          onChange={handleChange('password')}
          onKeyPress={handleKeyPress}
        />

        <Button
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
          onClick={userLogin}
        >
          Ingresar
        </Button>

        <Grid container className={classes.userFailed}>
          <Grid item xs className={classes.error}>
            {error}
          </Grid>
        </Grid>
      </div>
      <Box mt={8}>
        <Typography variant="body2" color="textSecondary" align="center">
          Copyright © EHO - Productions 2019.
        </Typography>
      </Box>
    </Container>
  )
}
