import React, { useEffect, useState, useContext } from 'react'
import { useSetAppState } from '../../context'
import jwt_decode from 'jwt-decode'
import Grid from '@material-ui/core/Grid'
import AppBar from '@material-ui/core/AppBar'
import CssBaseline from '@material-ui/core/CssBaseline'
import Divider from '@material-ui/core/Divider'
import Drawer from '@material-ui/core/Drawer'
import Hidden from '@material-ui/core/Hidden'
import IconButton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney'
import ListItemText from '@material-ui/core/ListItemText'
import HorizontalSplitIcon from '@material-ui/icons/HorizontalSplit'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import LocalShippingIcon from '@material-ui/icons/LocalShipping'
import MenuIcon from '@material-ui/icons/Menu'
import AccountBoxIcon from '@material-ui/icons/AccountBox'
import BuildsIcon from '@material-ui/icons/Build'
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import menuList from './components/FunctionalitiesTree'
import Layout from '../../components/Layout'
import { useHistory } from 'react-router-dom'
import {
  Stock,
  AddEmployed,
  Process,
  Commerce,
  CurrentAccounts,
  Orders,
} from '../../modules'
import { stockContext } from '../../context/stockContext'
import gralFunctions from '../../utils/gralFunctions'

const drawerWidth = 240

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  user: {
    float: 'right',
    textAlign: 'end',
  },
}))

function Home(props) {
  const { getStocks } = useContext(stockContext)
  const functionalities = menuList
  const { container } = props
  const classes = useStyles()
  const theme = useTheme()
  const [user, setUser] = useState(null)
  const [mobileOpen, setMobileOpen] = useState(false)
  const [myModule, setMyModule] = useState('')
  const history = useHistory()
  // const { userContext } = useAppState();

  const setAppState = useSetAppState()

  useEffect(() => {
    if (localStorage.getItem('usrToken')) {
      const decoded = jwt_decode(localStorage.getItem('usrToken'))
      setUser(decoded.user)
      setMyModule(localStorage.getItem('module') || '')
      localStorage.removeItem('module')
      getStocks()
    } else {
      history.push('/')
    }
  }, [])

  const logOut = () => {
    localStorage.clear()
    setUser(null)
    setMyModule(null)
    history.push('/')
  }

  const handleDrawerToggle = (props) => {
    setMobileOpen(!mobileOpen)
  }

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <List>
        {functionalities.menuList.map((text, index) => (
          <ListItem button onClick={() => setMyModule(text)} key={text}>
            <ListItemIcon>
              {text.includes('Stock') ? (
                <HorizontalSplitIcon />
              ) : text.includes('Empleados') ? (
                <AccountBoxIcon />
              ) : text.includes('corrientes') ? (
                <AttachMoneyIcon />
              ) : text.includes('Pedidos') ? (
                <LocalShippingIcon />
              ) : text.includes('Producc') ? (
                <BuildsIcon />
              ) : (
                <FormatListBulletedIcon />
              )}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
      <Divider />
      <List>
        <ListItem button onClick={logOut} key={'Salir'}>
          <ListItemIcon>
            <ArrowBackIcon />
          </ListItemIcon>
          <ListItemText primary={'Salir'} />
        </ListItem>
      </List>
    </div>
  )

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>

          <Grid item xs={12} sm={6}>
            <Typography variant="h6" noWrap>
              Sistema de Gestion
            </Typography>
          </Grid>
          <Grid item xs={12} sm={6} className={classes.user}>
            <Typography variant="h6" noWrap>
              {user
                ? user.name.charAt(0).toUpperCase() + user.name.slice(1)
                : 'no disponible'}
            </Typography>
          </Grid>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>

      <Layout>
        <div className={classes.toolbar}>
          {myModule === 'Stock M Prima' ? (
            <Stock mp key={Math.random} />
          ) : myModule === 'Stock Productos' ? (
            <Stock productos key={Math.random} />
          ) : myModule === 'Empleados' ? (
            <AddEmployed key={2} />
          ) : myModule === 'Clientes' ? (
            <Commerce clients key={2} />
          ) : myModule === 'Proveedores' ? (
            <Commerce providers key={2} />
          ) : myModule === 'Producción' ? (
            <Process process key={2} />
          ) : myModule.includes('corrientes') ? (
            <CurrentAccounts key={2} />
          ) : myModule.includes('edidos') ? (
            <Orders key={2} />
          ) : null}
        </div>
      </Layout>
    </div>
  )
}

export default Home

// const { users = [] } = useAppState();
// const setAppState = useSetAppState();

// const addMore = () => {
//   setAppState({ users: users.concat("Nuevo User") });
// };
