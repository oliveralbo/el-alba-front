import React, { Fragment } from 'react'
import { Form, Field } from 'react-final-form'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'

const useStyles = makeStyles((theme) => ({
  field: {
    // margin: theme.spacing(2),
    margin: '15px 0 15px 0',
    width: '100%',
    height: '37px',
    border: '1px solid #ccc',
    borderRadius: '4px',
    padding: '7px',
  },
  select: {
    margin: '15px 0 15px 0',
    // margin: theme.spacing(2),
    width: '100%',
    height: '37px',
    border: '1px solid #ccc',
    borderRadius: '4px',
    padding: '7px',
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  check: {
    marginTop: '2%',
  },
}))

function FormGral(props) {
  const classes = useStyles()
  const { title, assemble, onSubmit, selectedItem } = props
  return (
    <Fragment>
      {title ? <h1>{title}</h1> : null}
      <Form
        onSubmit={onSubmit}
        initialValues={
          selectedItem && {
            Proveedor: selectedItem.provider,
            Producto: selectedItem.product,
            quantity: selectedItem.quantity,
            valor: selectedItem.price && parseInt(selectedItem.price),
            tipo: selectedItem.tipo,
            name: selectedItem.name,
            email: selectedItem.email,
            surname: selectedItem.surname,
            dni: selectedItem.dni,
            phone: selectedItem.phone,
            role: selectedItem.role === 'admin',
            // password: selectedItem.password,
            // validatePassword: selectedItem.password,
          }
        }
        render={({ handleSubmit, form }) => (
          <form onSubmit={handleSubmit}>
            {assemble?.map((x) => {
              return (
                <div>
                  {x.component === 'field' ? (
                    <>
                      <Grid container>
                        <Grid item xs={12}>
                          <label>{x.label}</label>
                        </Grid>
                        <Grid item xs={12}>
                          <Field
                            className={classes.field}
                            name={x.name}
                            component="input"
                            type={x.type}
                            placeholder={x.placeholder}
                            required={x.required}
                            style={x.style}
                            fullWidth={x.fullWidth}
                            // onChange={x.onchange}
                          />
                        </Grid>
                      </Grid>
                    </>
                  ) : null}

                  {x.component === 'textarea' ? (
                    <>
                      <Grid container>
                        <Grid item xs={12}>
                          <label>{x.label}</label>
                        </Grid>
                        <Grid item xs={12}>
                          <Field
                            className={classes.field}
                            name={x.name}
                            component="textarea"
                            type={x.type}
                            placeholder={x.placeholder}
                            required={x.required}
                            style={x.style}
                          />
                        </Grid>
                      </Grid>
                    </>
                  ) : null}

                  {x.component === 'checkbox' ? (
                    <div className={classes.check}>
                      <label>
                        <Field
                          name={x.name}
                          component="input"
                          type={x.type}
                          value={x.value}
                          style={x.style}
                        />
                        {x.label}
                      </label>
                    </div>
                  ) : null}

                  {x.component === 'select' ? (
                    <div>
                      <Grid container>
                        <Grid item xs={12}>
                          <label>{x.label}</label>
                        </Grid>
                        <Grid item xs={12}>
                          <Field
                            name={x.name}
                            component="select"
                            placeholder={x.placeholder}
                            className={classes.select}
                          >
                            {x.options.map((item) => {
                              return <option value={item}>{item}</option>
                            })}
                          </Field>
                        </Grid>
                      </Grid>
                    </div>
                  ) : null}

                  {x.component === 'button' ? (
                    <Button
                      style={x.style}
                      type={x.type}
                      fullWidth={x.fullWidth}
                      variant={x.variant}
                      color={x.color}
                      className={classes.submit}
                      // onClick={userLogin}
                    >
                      {selectedItem ? 'Cambiar' : x.label}
                    </Button>
                  ) : null}
                </div>
              )
            })}
          </form>
        )}
      />
    </Fragment>
  )
}

export default FormGral
