import React, { useState, useEffect } from "react";
import MaterialTable from "material-table";

// ver si vale la pena este materialtable si no darle el unisnstall del proyecto

export default function MaterialTableDemo(props) {
  const { actionInsert, clients, providers, title, info } = props;

  const [state, setState] = useState({
    columns: [
      { title: "Nombre", field: "name" },
      { title: "Direccion", field: "address" },
      { title: "Telefono", field: "phone", type: "numeric" },
      { title: "Compania", field: "company" },
      { title: "Mail", field: "email" },
      { title: "Cta. corriente",field: "account"}
    ],
    data: []
  });

  useEffect(() => {
    load();
  }, [info]);

  const load = () => {
    setState({
      ...state,
      data: info ? info : []
    });

  };
  return (
    <MaterialTable
    title={title}
    columns={state.columns}
      data={state.data}
      editable={{
        onRowAdd: newData =>
        new Promise(resolve => {
          setTimeout(() => {
              resolve();
              setState(prevState => {
                const data = [...prevState.data];
                data.push(newData);
                actionInsert(newData);
                return { ...prevState, data };
              });
            }, 600);
          }),
          onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                setState(prevState => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState(prevState => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
          })
      }}
    />
  );
}
