import React from 'react'
import { Form } from 'react-final-form'
import arrayMutators from 'final-form-arrays'
import { FieldArray } from 'react-final-form-arrays'
import FormHelperText from '@material-ui/core/FormHelperText'
import { Grid, Typography } from '@material-ui/core'
import format from 'date-fns/format'
import { Autocomplete, TextField } from 'mui-rff'
import intervalToDuration from 'date-fns/intervalToDuration'
import CloseIcon from '@material-ui/icons/Close'
import AddIcon from '@material-ui/icons/Add'
import RemoveIcon from '@material-ui/icons/Remove'
import Fab from '@material-ui/core/Fab'
// cambiar el objeto pedido y vincularlo con clientes y
//productos nos permitira limpiar esto y hacer mas facil su funcionamiento
export default function OrderForm(props) {
  const { clients, products, discount, id, saveOrder, setTotal, onSubmit } =
    props
  const date = new Date()
  const dateSplit = saveOrder && saveOrder.orderDate.split('/')
  const interval =
    dateSplit &&
    intervalToDuration({
      start: new Date(dateSplit[2], Number(dateSplit[1]) - 1, dateSplit[0]),
      end: new Date(),
    })
  return (
    <Form
      style={{ width: '100%' }}
      onSubmit={onSubmit}
      initialValues={
        saveOrder && {
          client: {
            name: saveOrder.clientName,
            _id: saveOrder.clientId,
          },
          arrayProducts: saveOrder.product.map((element, index) => {
            return {
              producto: element,
              cantidad: saveOrder.quantity[index],
            }
          }),
        }
      }
      mutators={{
        ...arrayMutators,
      }}
      render={({
        handleSubmit,
        form: {
          mutators: { push, pop },
        },
        pristine,
        form,
        submitting,
        initialValues,
        values,
      }) => {
        return (
          <form onSubmit={handleSubmit} style={{ width: '100%' }}>
            <Grid container spacing={8}>
              <Grid item xs={7}>
                <FormHelperText>Cliente</FormHelperText>
                <Autocomplete
                  key={(client) => client._id}
                  size="small"
                  style={{ width: '50%' }}
                  name={`client`}
                  required
                  disabled={!!initialValues}
                  options={clients}
                  getOptionValue={(client) => client}
                  getOptionLabel={(client) => client.name}
                  renderInput={(params) => {
                    initialValues && delete params.inputProps.value
                    return (
                      <TextField
                        {...params}
                        name={`client.name`}
                        size="small"
                        variant="outlined"
                      />
                    )
                  }}
                />
              </Grid>
              <Grid item xs={3}>
                <Typography variant="overline">
                  {!!saveOrder ? (
                    <>
                      <Typography
                        variant="caption"
                        display="block"
                        gutterBottom
                      >
                        Pedido tomado:{' '}
                      </Typography>
                      <div
                        style={{
                          color:
                            interval.days > 7 ||
                            interval.months > 0 ||
                            interval.years > 0
                              ? 'rgb(221 79 79)'
                              : '',
                        }}
                      >
                        <b>{saveOrder.orderDate}</b>
                      </div>
                      <div>{format(date, 'dd/MM/yyyy')}</div>
                    </>
                  ) : (
                    <div>{format(date, 'dd/MM/yyyy')}</div>
                  )}
                </Typography>
              </Grid>
              <Grid item xs={2}>
                <CloseIcon
                  style={{ marginLeft: '15px' }}
                  onClick={() => {
                    discount(id)
                  }}
                />
              </Grid>
            </Grid>
            <Grid
              container
              justifyContent="space-between"
              style={{ marginTop: '24px' }}
            >
              <label style={{ width: '25%' }}>Producto: </label>
              {!initialValues && (
                <>
                  <label style={{ width: '100px' }}>cantidad:</label>
                  <label style={{ width: '15%' }}>Precio x kg:</label>
                </>
              )}
              <label style={{ marginLeft: '1%' }}>
                {initialValues ? 'Cantidad' : 'Subtotoal:'}
              </label>
              <FieldArray name="arrayProducts">
                {({ fields }) =>
                  fields.map((name, index) => {
                    const producto = values?.arrayProducts[index]?.producto
                    const total = values?.arrayProducts.reduce(
                      (accumulator, currentValue) => {
                        const itemValue =
                          currentValue?.cantidad * currentValue?.producto?.price
                        return accumulator + itemValue || accumulator
                      },
                      0
                    )
                    initialValues = !!initialValues?.arrayProducts
                      ? initialValues
                      : null
                    setTotal(total || saveOrder?.amount)
                    return (
                      <div
                        key={
                          !products
                            ? `${initialValues.arrayProducts[index].producto}${index}`
                            : products[index]?._id
                        }
                        style={{ width: '100%' }}
                      >
                        <Grid
                          item
                          xs={12}
                          style={{ display: 'flex' }}
                          justifyContent="space-between"
                        >
                          <Autocomplete
                            size="small"
                            style={{ width: '25%' }}
                            name={
                              initialValues ? 'producto' : `${name}.producto`
                            }
                            label="producto"
                            disabled={
                              !!initialValues && !!initialValues.arrayProducts
                            }
                            options={products && products}
                            getOptionValue={(x) => x}
                            getOptionLabel={(x) => x.product || ''}
                            renderInput={(params) => {
                              if (
                                !!initialValues &&
                                !!initialValues.arrayProducts
                              ) {
                                params.inputProps.value =
                                  initialValues.arrayProducts[index].producto
                              }
                              return (
                                <TextField
                                  {...params}
                                  name={`${name}.producto`}
                                  size="small"
                                  variant="outlined"
                                />
                              )
                            }}
                          />
                          <TextField
                            InputProps={{ inputProps: { min: 0 } }}
                            size="small"
                            label="cant"
                            disabled={!!initialValues}
                            name={`${name}.cantidad`}
                            type="number"
                            required={true}
                            style={{ maxWidth: '100px' }}
                          />
                          {!initialValues && (
                            <>
                              <span style={{ marginLeft: '1%', width: '15%' }}>
                                {producto && (producto?.price || 0)}
                              </span>
                              <div>
                                <span style={{ textAlign: 'end' }}>
                                  {producto &&
                                  values.arrayProducts[index].cantidad
                                    ? values?.arrayProducts[index]?.cantidad *
                                      producto.price
                                    : '---'}
                                </span>

                                <span
                                  onClick={() => fields?.remove(index)}
                                  style={{ cursor: 'pointer' }}
                                >
                                  ❌
                                </span>
                              </div>
                            </>
                          )}
                        </Grid>
                      </div>
                    )
                  })
                }
              </FieldArray>
              {!initialValues && (
                <>
                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                    }}
                  >
                    <Fab
                      color="primary"
                      onClick={() => push('arrayProducts', undefined)}
                    >
                      <AddIcon />
                    </Fab>
                    <Fab color="primary" onClick={() => pop('arrayProducts')}>
                      <RemoveIcon />
                    </Fab>
                  </div>
                </>
              )}

              <div className="buttons">
                <button type="submit" disabled={submitting || pristine}>
                  Submit
                </button>
                <button
                  type="button"
                  onClick={form.reset}
                  disabled={submitting || pristine}
                >
                  Reset
                </button>
              </div>
            </Grid>
          </form>
        )
      }}
    />
  )
}
