import React from 'react'
import OrderForm from './OrderForm'
import { makeStyles } from '@material-ui/core/styles'
import { Paper } from '@material-ui/core'
import format from 'date-fns/format'
import services from '../../api/services'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(4),
    margin: 'auto',
    backgroundColor: '#EAEDED',
    width: '900px',
    marginBottom: '2%',
    // display: 'flex',
    justifyContent: 'space-between',
  },
}))

export default function OrderCard2(props) {
  const { clients, products, discount, id, initialValues, refresh } = props
  const [total, setTotal] = React.useState(0)

  const onSubmit = async (values) => {
    //aca van los servcios de la entrega del pedido
    console.log(values)
  }

  // const handleChangeClient = (event) => {
  //   let values = event.target.value.split('-')
  // }

  const onSave = async (values) => {
    let pedido = {}
    pedido.orderDate = format(new Date(), 'dd/MM/yyyy')
    pedido.deliveryDate = ''
    pedido.amount = total
    pedido.product = []
    pedido.quantity = []
    //pasar objeto cliente  (ver tarjeta trello de back)
    pedido.clientId = values.client._id
    pedido.clientName = values.client.name
    values.arrayProducts.forEach((item) => {
      //pasar objeto productos  (ver tarjeta trello de back)
      pedido.product.push(item.producto.product)
      pedido.quantity.push(item.cantidad)
    })
    //aca descontar producto de venta
    try {
      await services.saveOrder(pedido)
      refresh()
      discount(id)
    } catch (e) {
      console.log('error: ' + e)
    }
  }

  const classes = useStyles()
  return (
    <Paper className={classes.paper}>
      <OrderForm
        key={id}
        saveOrder={initialValues}
        products={products}
        clients={clients}
        discount={discount}
        id={id}
        onSubmit={onSave}
        refresh={refresh}
        setTotal={setTotal}
      />
      {total > 0 && (
        <Typography
          variant="h4"
          gutterBottom
          style={{ display: 'flex', justifyContent: 'flex-end' }}
        >
          Total: ${total}
        </Typography>
      )}
    </Paper>
  )
}
