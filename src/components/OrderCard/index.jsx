import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import intervalToDuration from 'date-fns/intervalToDuration'
import Typography from '@material-ui/core/Typography'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import services from '../../api/services'
import format from 'date-fns/format'
import FormHelperText from '@material-ui/core/FormHelperText'
import AddIcon from '@material-ui/icons/Add'
import RemoveIcon from '@material-ui/icons/Remove'
import TextField from '@material-ui/core/TextField'
import Fab from '@material-ui/core/Fab'
import CloseIcon from '@material-ui/icons/Close'
import Button from '@material-ui/core/Button'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(4),
    margin: 'auto',
    backgroundColor: '#EAEDED',
    maxWidth: '873px',
    marginBottom: '2%',
  },
  firstGrid: {
    padding: '0!important',
  },
  icon: {
    width: '16%',
    height: '11%',
    marginLeft: '40%',
    marginTop: '10%',
  },
  date: {
    display: 'flex!important',
    paddingRight: '2%!important',
    paddingLeft: '35%!important',
  },
  select: {
    width: '200px',
    float: 'left',
  },
  select2: {
    width: '230px',
    float: 'left',
  },
  rows: {
    display: 'flex',
    padding: '2%',
  },
  row1: {
    width: '44%',
  },
  row2: {
    width: '15%',
  },
  row3: {
    width: '33%',
    textAlign: 'center',
  },
  row4: {
    width: '10%',
    textAlign: 'center',
  },
  row3Child: {
    marginTop: '6%',
  },
  row4Child: {
    marginTop: '21%!important',
  },
  total: {
    paddingLeft: '17%',
  },
  entregado: {
    marginLeft: '50%',
  },
}))

export default function OrderCard(props) {
  const { clients, products, discount, id, saveOrder, refresh } = props
  const classes = useStyles()

  const [quantityProd, setQuantityProd] = useState([1])
  const [selectedProds, setSelectedProds] = useState([])
  const [selCantProd, setSelCantProd] = useState([])
  const [total, setTotal] = useState(0)
  const [errorSelects, setErrorSelects] = useState(false)
  const [clientId, setClientId] = useState(null)
  const [clientName, setClientName] = useState(null)
  const [orderId, setOrderId] = useState(null)
  const date = new Date()

  useEffect(() => {
    if (saveOrder) {
      saveOrder.product.map((product) => {
        setQuantityProd([...quantityProd, 1])
      })
      setSelectedProds(saveOrder.product)
      setSelCantProd(saveOrder.quantity)
      setClientId(saveOrder.clientId)
      setClientName(saveOrder.clientName)
      setOrderId(saveOrder._id)
    }
  }, [saveOrder])

  useEffect(() => {
    let acumulador = 0
    for (let i = 0; i < selectedProds.length; i++) {
      let totalProd = selectedProds && selectedProds[i].price * selCantProd[i]
      acumulador = acumulador + totalProd
    }
    setTotal(acumulador)
  }, [selCantProd])

  const addSelect = () => {
    if (
      quantityProd.length === selCantProd.length &&
      quantityProd.length === selectedProds.length
    ) {
      setQuantityProd([...quantityProd, 1])
    } else {
      setErrorSelects(true)
      setTimeout(() => setErrorSelects(false), 800)
    }
  }

  const removeSelect = () => {
    let quantAux = [...selectedProds]
    setQuantityProd(quantAux)
  }

  const handleChangeClient = (event) => {
    let values = event.target.value.split('-')
    setClientId(values[0])
    setClientName(values[1])
  }

  const handleChangeProd = (event) => {
    let prodsCopy = [...selectedProds]
    prodsCopy[event.target.name] = event.target.value
    setSelectedProds(prodsCopy)
  }

  const handleChangeCantProd = (event) => {
    let index = event.target.name

    let testNumber = Number(event.target.value)

    if (testNumber > 0) {
      let cantMpsCopy = [...selCantProd]
      cantMpsCopy[index] = event.target.value
      setSelCantProd(cantMpsCopy)
    } else {
      let cantMpsCopy = [...selCantProd]
      cantMpsCopy.splice(index, 1)
      setSelCantProd(cantMpsCopy)
    }
  }

  const handleSaveOrder = async () => {
    let pedido = {}
    pedido.orderDate = format(date, 'dd/MM/yyyy')
    pedido.deliveryDate = ''
    pedido.amount = total
    pedido.product = []
    pedido.quantity = []
    pedido.clientId = clientId
    pedido.clientName = clientName
    selectedProds.map((prod) => {
      pedido.product.push(prod.product)
    })
    selCantProd.map((cant) => {
      pedido.quantity.push(cant)
    })

    try {
      await services.saveOrder(pedido)
      refresh()
      discount(id)
    } catch (e) {
      console.log('error: ' + e)
    }
  }

  const handleAction = async () => {
    let pedido = {}
    pedido.orderDate = format(date, 'dd/MM/yyyy')
    pedido.delivered = true
    try {
      await services.deliveryUser(saveOrder.amount, clientId)
      await services.deliveryOrder(pedido, orderId)
      // await services.descontar producto hacer
      refresh()
    } catch (e) {
      console.log('error: ' + e)
    }
  }
  const dateSplit = saveOrder && saveOrder.orderDate.split('/')
  const interval =
    dateSplit &&
    intervalToDuration({
      start: new Date(dateSplit[2], Number(dateSplit[1]) - 1, dateSplit[0]),
      end: new Date(),
    })

  return (
    <Paper className={classes.paper}>
      <Grid container spacing={8}>
        <Grid item xs={6}>
          <FormHelperText>Cliente</FormHelperText>
          {saveOrder ? (
            <TextField
              disabled={true}
              id="standard-number"
              type="text"
              name="cliente"
              value={saveOrder && saveOrder.clientName}
            />
          ) : (
            <Select
              disabled={false}
              labelId="demo-simple-select-label"
              label="Cliente"
              id="demo-simple-select"
              onChange={handleChangeClient}
              className={classes.select}
              value={saveOrder && saveOrder.clientName}
            >
              {clients &&
                clients.map((cliente) => {
                  return (
                    <MenuItem
                      value={cliente._id + '-' + cliente.name}
                      name={cliente.name}
                      key={cliente._id}
                    >
                      {cliente.name}
                    </MenuItem>
                  )
                })}
            </Select>
          )}
        </Grid>
        <Grid item xs={6} className={classes.date}>
          <Typography variant="overline">
            {saveOrder ? (
              <>
                <Typography variant="caption" display="block" gutterBottom>
                  Pedido tomado:{' '}
                </Typography>
                <div
                  style={{ color: interval.days > 7 ? 'rgb(221 79 79)' : '' }}
                >
                  <b>{saveOrder.orderDate}</b>
                </div>
                <div>{format(date, 'dd/MM/yyyy')}</div>
              </>
            ) : (
              <div>{format(date, 'dd/MM/yyyy')}</div>
            )}
          </Typography>
          <CloseIcon
            style={{ marginLeft: '15px' }}
            onClick={(event) => {
              discount(saveOrder ? orderId : id)
            }}
          />
        </Grid>

        <Grid xs={8} className={classes.rows}>
          <div className={classes.row1}>
            <Typography variant="subtitle2" gutterBottom>
              Producto
            </Typography>
            {saveOrder
              ? selectedProds.map((item, index) => {
                  return (
                    <Select
                      disabled={true}
                      error={
                        quantityProd.length - 1 === index ? errorSelects : false
                      }
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      onChange={handleChangeProd}
                      className={classes.select2}
                      name={index}
                      value={item}
                    >
                      {selectedProds.map((producto) => {
                        return (
                          <MenuItem key={Math.random} value={producto}>
                            {producto}
                          </MenuItem>
                        )
                      })}
                    </Select>
                  )
                })
              : quantityProd.map((item, index) => {
                  return (
                    <Select
                      disabled={false}
                      error={
                        quantityProd.length - 1 === index ? errorSelects : false
                      }
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      onChange={handleChangeProd}
                      className={classes.select2}
                      name={index}
                    >
                      {products &&
                        products.map((x) => {
                          return (
                            <MenuItem key={Math.random} value={x}>
                              {x.product}
                            </MenuItem>
                          )
                        })}
                    </Select>
                  )
                })}
            <div
              style={{
                display: 'flex',
                flexDirection: 'row',
                width: '150%',
              }}
            >
              <Fab color="primary" className={classes.icon} onClick={addSelect}>
                <AddIcon />
              </Fab>
              <Fab
                color="primary"
                className={classes.icon}
                onClick={removeSelect}
              >
                <RemoveIcon />
              </Fab>
            </div>
          </div>

          <div className={classes.row2}>
            <Typography
              variant="subtitle2"
              gutterBottom
              color={'textSecondary'}
            >
              Cantidad.
            </Typography>
            {saveOrder
              ? selCantProd.map((item, index) => {
                  return (
                    <TextField
                      disabled={saveOrder ? true : false}
                      error={
                        quantityProd.length - 1 === index ? errorSelects : false
                      }
                      id="standard-number"
                      type="number"
                      onChange={handleChangeCantProd}
                      InputLabelProps={{
                        shrink: true,
                      }}
                      name={index}
                      value={item}
                    />
                  )
                })
              : quantityProd.map((item, index) => {
                  return (
                    <TextField
                      disabled={saveOrder ? true : false}
                      error={
                        quantityProd.length - 1 === index ? errorSelects : false
                      }
                      id="standard-number"
                      type="number"
                      onChange={handleChangeCantProd}
                      InputLabelProps={{
                        shrink: true,
                      }}
                      name={index}
                    />
                  )
                })}
          </div>

          <div className={classes.row3}>
            <Typography
              variant="subtitle2"
              gutterBottom
              color={'textSecondary'}
            >
              Precio por Kg.
            </Typography>
            {selectedProds.map((item, index) => {
              return (
                <Typography
                  variant="body1"
                  gutterBottom
                  name={index}
                  className={classes.row3Child}
                >
                  ${selectedProds[index] && selectedProds[index].price}
                </Typography>
              )
            })}
          </div>

          <div className={classes.row4}>
            <Typography
              variant="subtitle2"
              gutterBottom
              color={'textSecondary'}
            >
              Subtotal
            </Typography>
            {quantityProd.map((item, index) => {
              let aux =
                selectedProds[index] &&
                selCantProd &&
                selectedProds[index].price * selCantProd[index]
              return (
                <Typography
                  variant="body1"
                  gutterBottom
                  name={index}
                  className={classes.row4Child}
                >
                  {isNaN(aux) ? '-' : aux}
                </Typography>
              )
            })}
          </div>
        </Grid>

        <Grid xs={4}>
          <Grid item md={4} className={classes.total}>
            <Typography variant="h4" gutterBottom align={'end'}>
              TOTAL
            </Typography>

            <Typography variant="h4" gutterBottom align={'end'}>
              ${saveOrder ? saveOrder.amount : total}
            </Typography>
          </Grid>
          <Grid item md={4} className={classes.entregado}>
            {!saveOrder ? (
              <Button variant="contained" onClick={handleSaveOrder}>
                Guardar
              </Button>
            ) : (
              <Button variant="contained" onClick={handleAction}>
                Entregar
              </Button>
            )}
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  )
}
