import React from "react";
import Grid from "@material-ui/core/Grid";
import { Container } from "@material-ui/core";
import Box from "@material-ui/core/Box";

const styles = {
  cont: {
    marginBottom: "20px"
  }
}

function Layout(props) {
  return (
    <Box mt={10}>
      <Container>
        <Grid container style={styles.cont}>{props.children}</Grid>
      </Container>
    </Box>
  );
}

export default Layout;
