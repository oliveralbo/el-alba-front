import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import Grid from "@material-ui/core/Grid";
import Button from '@material-ui/core/Button';
import services from "../../api/services";

const useStyles = makeStyles(theme => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)

  },
  button: {
    margin: "5%"
  }
}));

export default function ModalGral(props) {
  const { open, handleClose, selectedItem, message, title, action, objectValues } = props;
  const classes = useStyles();




  return (
    <>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <h3 id="transition-modal-title">{title}</h3>
            <Grid item mb={2}>
              {message}
            </Grid>
              <div style={{display:"flex", justifyContent: 'center'}}>
                <Button className={classes.button} variant="contained" onClick={()=>action(objectValues)}>aceptar</Button>
                <Button className={classes.button} variant="contained" onClick={handleClose}>cancelar</Button>
              </div>
          </div>
        </Fade>
      </Modal>
    </>
  );
}
