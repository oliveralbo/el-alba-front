import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActionArea from "@material-ui/core/CardActionArea";
import Typography from "@material-ui/core/Typography";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import { Grid } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";

const useStyles = makeStyles({
  card: {
    minWidth: 275
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
});

export default function CardGral(props) {
  const { data, action, Handledelete, type } = props;
  const classes = useStyles();

  return (
    <Card
      className={classes.card}
      style={
        data.quantity > 5
          ? { background: "lavender" }
          : { background: "#FB9B9B" }
      }
    >
      <CardActionArea onClick={action}>
        <CardContent>
          <Typography
            component="h3"
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
            Producto: <b>{data.product ? data.product : 0}</b>
          </Typography>
          <Typography variant="h5" component="h2">
            Cantidad: {data.quantity ? data.quantity : 0} 
          </Typography>
         
          <Typography className={classes.pos} color="textSecondary">
          {data.tipo}
          </Typography>
         
          <Grid item>
            <Typography variant="body2" component="p" style={{ float: "left" }}>
              {`Precio de ${type === "mp" ? 'compra' : type === "productos" ? 'venta' : `por${data.tipo} `}`}
              <br />${data.price || " --"}
            </Typography>
            <Tooltip title="Delete">
              <IconButton
                aria-label="delete"
                style={{ float: "right" }}
                onClick={Handledelete}
              >
                <DeleteIcon />
              </IconButton>
            </Tooltip>
          </Grid>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
