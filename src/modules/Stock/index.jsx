import React, { useState, useEffect, useContext } from 'react'
import CardGral from '../../components/Cards'
import Box from '@material-ui/core/Box'
import services from '../../api/services'
import { Grid } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import Fab from '@material-ui/core/Fab'
import StockModal from './components/StockModal'
import AlertDialog from '../../components/AlertDialog'
import { stockContext } from '../../context/stockContext'

const style = {
  margin: 5,
  top: 'auto',
  right: 20,
  bottom: 20,
  left: 'auto',
  position: 'fixed',
}

export default function Stock(props) {
  const { mp, productos } = props
  // const contextStock = useContext(stockContext)
  const { contextMp, contextProd, getStocks } = useContext(stockContext)
  const [data, setData] = useState(null)
  const [openModal, setOpenModal] = useState(false)
  const [openAlert, setOpenAlert] = useState(false)
  const [selectedItem, setSelectedItem] = useState(null)
  const [refresh, setRefresh] = useState(0)

  useEffect(() => {
    setData(mp ? contextMp : productos ? contextProd : null)
  }, [mp, productos, refresh, contextProd, contextMp])

  const handleRefresh = async () => {
    await getStocks()
    setRefresh(refresh + 1)
  }

  const handleOpenEdit = (e, x) => {
    e.preventDefault()
    setSelectedItem(x)
    setOpenModal(true)
  }

  const handleOpenModal = () => {
    setSelectedItem(null)
    setOpenModal(true)
  }

  const handleClose = () => {
    setOpenModal(false)
  }

  const handleCloseAlert = () => {
    setOpenAlert(false)
  }

  const Handledelete = (e, x) => {
    e.preventDefault()
    e.stopPropagation()
    setSelectedItem(x)
    setOpenAlert(true)
  }

  const deleteItem = async (e) => {
    e.preventDefault()
    e.stopPropagation()
    console.log(selectedItem)
    try {
      const deleteObject = mp
        ? await services.deleteRawMaterial(selectedItem._id)
        : productos
        ? await services.deleteProduct(selectedItem._id)
        : null
      console.log(deleteObject)
    } catch (e) {
      console.error(e)
    }
    handleCloseAlert()
    handleRefresh()
  }

  return (
    <>
      <h1>Stock {mp ? 'Materia Prima' : 'de Venta'}</h1>
      <Grid container>
        {data &&
          data.map((x) => {
            return (
              <Box mr={2} mt={2}>
                <CardGral
                  data={x}
                  action={(e) => handleOpenEdit(e, x)}
                  Handledelete={(e) => Handledelete(e, x)}
                  type={mp ? 'mp' : productos ? 'productos' : null}
                />
              </Box>
            )
          })}
      </Grid>

      <Fab color="primary" style={style} onClick={handleOpenModal}>
        <AddIcon />
      </Fab>

      <StockModal
        type={mp ? 'mp' : 'productos'}
        handleOpen={openModal}
        handleClose={handleClose}
        selectedItem={selectedItem}
        refresh={handleRefresh}
      />
      <AlertDialog
        selectedItem={selectedItem}
        openAlert={openAlert}
        handleCloseAlert={handleCloseAlert}
        deleteItem={(e) => deleteItem(e)}
      />
    </>
  )
}
