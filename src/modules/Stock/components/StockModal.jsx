import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import FormGral from '../../../components/FormGral'
import Grid from '@material-ui/core/Grid'
import services from '../../../api/services'
import { Box } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #f3f3f3',
    borderRadius: '5px',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    
  }
}));

export default function StockModal(props) {
  const { handleOpen, handleClose, selectedItem, type, refresh } = props
  const classes = useStyles()

  const assembleMp = [
    {
      component: 'field',
      type: 'text',
      required: true,
      name: "Producto",
      placeholder: "Producto",
      label: "Producto",
      fullWidth: true,
    },
    {
      component: 'field',
      type: 'text',
      required: true,
      name: 'quantity',
      placeholder: 'cantidad',
      label: 'cantidad',
    },
    {
      component: 'field',
      type: 'number',
      // required: true,
      name: "valor",
      placeholder: "valor",
      label: "Precio de compra(optativo)",
      fullWidth: true,
    },
    {
      component: 'select',
      required: true,
      name: "tipo",
      placeholder: "Tipo",
      label: "Ingrese tipo",
      options: ["Kilogramos", "Litros"],
      fullWidth: true,
    },
    {
      component: 'button',
      type: 'submit',
      fullWidth: true,
      variant: 'contained',
      color: 'primary',
      label: 'Agregar',
      style: {
        backgroundColor: '#ffffff',
        color: 'black',
        border: 'solid 1px black',
      },
    },
  ]

  const assembleProd = [
    {
      component: 'field',
      type: 'text',
      required: true,
      name: 'Producto',
      placeholder: 'Producto',
      label: 'Producto',
    },
    {
      component: 'field',
      type: 'number',
      required: true,
      name: 'valor',
      placeholder: 'Valor de venta',
      label: 'Valor de venta',
    },
    {
      component: 'select',
      required: true,
      name: "tipo",
      placeholder: "Tipo",
      label: "Ingrese tipo",
      options: ["Kilogramos", "Litros"],
      fullWidth: true,
    },
    {
      component: 'button',
      type: 'submit',
      fullWidth: true,
      variant: 'contained',
      color: 'primary',
      label: 'Agregar',
      style: {
        backgroundColor: '#ffffff',
        color: 'black',
        border: 'solid 1px black',
      },
    },
  ]

  const onSubmit = async (values) => {
    let data = {}
    try {
      data.product = values.Producto
      data.quantity = Number(values.quantity) || 0
      data.tipo = values.tipo || 'Kilogramos'
      data.price = values.valor
      if (selectedItem) {
        type === 'mp'
          ? await services.updateRawMaterial(selectedItem._id, data)
          : await services.updateProduct(data)
      } else {
        type === 'mp'
          ? await services.addRawMaterial(data)
          : await services.addProduct(data)
      }
      handleClose()
      refresh()
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={handleOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={handleOpen}>
          <Box className={classes.paper}>
            <h3 id="transition-modal-title">
              Ingrese {type === 'productos' ? 'Producto' : 'materia prima'}
            </h3>
            <Grid item xs={12}>
              <FormGral
                assemble={type === 'mp' ? assembleMp : assembleProd}
                onSubmit={onSubmit}
                selectedItem={selectedItem}
              />
            </Grid>
          </Box>
        </Fade>
      </Modal>
    </>
  )
}
