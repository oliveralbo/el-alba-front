import React, { useState, useEffect } from "react";
import Table from "../../components/Table";
import services from "../../api/services";

export default function Commerce(props) {
  const { clients, providers } = props;
  const [data, setData] = useState(null);

  useEffect(() => {
    getAll();
  }, [clients, providers]);

  const getAll = async () => {
    try {
      const data = clients
        ? await services.getClients()
        : providers
          ? await services.getProviders()
          : null;
      // data.data ? data.data.records : data);
      setData(data.data ? data.data.records : data);
    } catch (e) {
      console.log(e);
    }
  };

  const actionInsert = x => {
    console.log(JSON.stringify(x) + "se han insertado los datos");
  };

  return (
    <>
      <Table
        title={clients ? "Clientes" : "Proveedores"}
        actionInsert={actionInsert}
        info={data}
      />
    </>
  );
}
