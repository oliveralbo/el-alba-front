import React, { useContext, useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from "@material-ui/core/Typography";
import Grid from '@material-ui/core/Grid';
import * as moment from 'moment'
import Moment from 'react-moment';

import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import services from "../../api/services";
import PaymentsTable from '../../components/PaymentsTable'
import FormGral from "../../components/FormGral";



const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        height: "200px"
    },
    select: {
        width: "200px",
        float: "right"
    },
    selectTitle: {
        textAlign: "right"
    },
    mount: {
        textAlign: "center"
    },
    historial: {
        marginTop: "3%"
    }

}));

export default function CurrentAccounts() {
    const classes = useStyles();
    const [clients, setClients] = useState([])
    const [client, setClient] = useState(null)
    const [payments, setPayments] = useState(null)
    const [paymentId, setPaymentId] = useState(0)
    let date = new Date()


    const assemble = [
        {
            component: "textarea",
            type: "text",
            required: true,
            name: "comentario",
            placeholder: "Comentario",
            style: { height: "85px", width: "90%" }
            // label: "Comentario"
        },
        {
            component: "field",
            type: "number",
            required: true,
            name: "pago",
            placeholder: "$ pago",
            style: {
                width: "50%",
                float: "left"
            }
            // label: "pago"
        },
        {
            component: "button",
            type: "submit",
            fullWidth: true,
            variant: "contained",
            color: "primary",
            label: "Pagar",
            style: {
                backgroundColor: "#ffffff",
                color: "black",
                border: "solid 1px black",
                width: "30%"
            }
        }
    ];



    useEffect(() => {
        getClients();
    }, []);


    useEffect(() => {
        if (paymentId > 0) {
            newPayUpdate(client._id)
        }
    }, [paymentId])

    const getClients = async () => {
        try {
            const data = await services.getClients()
            setClients(data.data ? data.data.records : data);
        } catch (e) {
            console.log(e);
        }
    };

    const handleChangeClient = async (event) => {
        let id = event.target && event.target.value ? event.target.value : client._id

        newPayUpdate(id)

    }

    const newPayUpdate = async (id) => {
        try {
            const client = await services.getClientById(id)
            setClient(client.data ? client.data.registry : client);
            const payments = await services.getPayments(id)
            setPayments(payments.data ? payments.data.registry : payments)
        } catch (e) {
            console.log(e);
        }
    }

    const onSubmit = async values => {
        let data = {}
        let dia = moment(date).format('DD/MM/YYYY')
        data.day = dia
        data.amount = values.pago
        data.coments = values.comentario
        data._id = client._id

        try {
            const payment = await services.addPayment(data)
            setPaymentId(paymentId + 1)
        } catch (e) {
            console.log(e);
        }

    };


    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <Typography variant="h6" gutterBottom>
                        <Moment format="DD/MM/YYYY">
                            {date}
                        </Moment>
                    </Typography>
                </Grid>
                <Grid item xs={6} >
                    <InputLabel className={classes.selectTitle} id="demo-simple-select-label">Seleccione cliente</InputLabel>
                    < Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        onChange={handleChangeClient}
                        className={classes.select}
                    // value={client ? client.name : null}
                    >
                        {clients && clients.map(cliente => {
                            return <MenuItem value={cliente._id}>{cliente.name}</MenuItem>
                        })
                        }
                    </Select>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="h2" gutterBottom className={classes.mount}>
                        ${client && client.account ? client.account : " -"}
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <PaymentsTable payments={payments} />
                </Grid>

                <Grid item xs={6} md={6}>
                    <div className={classes.paper}>
                        <FormGral
                            title={null}
                            assemble={assemble}
                            onSubmit={onSubmit}
                        />
                    </div>
                </Grid>
                <Grid item xs={6} className={classes.historial}>
                    <a href="#">historial de pedidos</a>
                </Grid>

            </Grid>
        </div>
    );
}
