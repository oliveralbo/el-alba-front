import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import services from '../../api/services'
// import OrderCard from '../../components/OrderCard'
import OrderCard2 from '../../components/OrderCard/OrderCard2'
import Grid from '@material-ui/core/Grid'
import AddIcon from '@material-ui/icons/Add'
import Fab from '@material-ui/core/Fab'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
}))

export default function Orders() {
  const classes = useStyles()
  const [clients, setClients] = useState([])
  const [products, setProducts] = useState(null)
  const [quantityCards, setQuantityCards] = useState([1])
  const [orders, setOrders] = useState(null)
  const [refresh, setRefresh] = useState(0)

  useEffect(() => {
    getAll()
  }, [])

  useEffect(() => {
    getAll()
  }, [refresh])

  const getAll = async () => {
    try {
      const ordersUndelivered = await services.getOrdersUndelivered()
      setOrders(
        ordersUndelivered.data
          ? ordersUndelivered.data.registry
          : ordersUndelivered
      )
      const clients = await services.getClients()
      setClients(clients.data ? clients.data.records : clients)
      const products = await services.getAllProductDone()
      setProducts(products.data ? products.data.records : products)
    } catch (e) {
      console.log(e)
    }
  }

  const addOrder = () => {
    setQuantityCards([...quantityCards, 1])
  }

  const discountCard = (e) => {
    const ordersAux = [...quantityCards]
    ordersAux.splice(e, 1)
    setQuantityCards(ordersAux)
  }

  const removeOrder = async (id) => {
    await services.deleteOrders(id)
    handleRefresh()
  }

  const handleRefresh = () => {
    setRefresh(refresh + 1)
  }

  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        {orders &&
          orders.map((order, i) => {
            return (
              <OrderCard2
                key={order.id}
                initialValues={order}
                products={products}
                clients={clients}
                discount={removeOrder}
                refresh={handleRefresh}
              />
            )
          })}
        {quantityCards.map((x, i) => {
          return (
            <OrderCard2
              key={x + 100}
              products={products}
              clients={clients}
              discount={discountCard}
              id={i}
              refresh={handleRefresh}
            />
          )
        })}
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
          }}
        >
          <Fab color="primary" className={classes.icon} onClick={addOrder}>
            <AddIcon />
          </Fab>
        </div>
      </Grid>
    </Grid>
  )
}
