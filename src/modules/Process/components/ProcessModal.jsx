import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    // display:"flex"
  },
  buttons: {
    display: "flex",
    justifyContent: "space-around",
    marginTop:"32px"
  },
}));

export default function ProcessModal(props) {
  const {
    handleOpen,
    handleClose,
    receivedObject,
    title,
    action,
    usage,
    mps,
    prod,
    msg,
  } = props;
  const classes = useStyles();

  const message = () => {
    let cants = receivedObject && Object.values(receivedObject?.mp);
    let result = [];
    for (let i = 0; i < cants?.length; i++) {
      result.push(`${cants[i]} de ${mps[i]}`);
    }
    return (
      <Grid item mb={2}>
        <p>{`Desea usar ${result.map((x) => {
          return x;
        })} para hacer`}</p>
        <p>
          {`${prod} - ${
            receivedObject && Object.values(receivedObject.pro)
          } Kg`}
        </p>
      </Grid>
    );
  };

  return (
    <>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={handleOpen}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={handleOpen}>
          <div className={classes.paper}>
            <h3 id="transition-modal-title">{title}</h3>
            {usage === "error" || usage === "accept" ? (
              <Grid item mb={2}>
                <div dangerouslySetInnerHTML={{ __html: msg }} />
              </Grid>
            ) : usage === "deleteRecipe" ? 
            <Grid item mb={2}>
                 <div dangerouslySetInnerHTML={{ __html: msg }} />
              </Grid>
            
            
            :(
              <Grid item mb={2}>
                <p>{message()}</p>
              </Grid>
            )}

            <Grid item className={classes.buttons}>
              <Button
                variant="contained"
                onClick={()=>{
                  usage === "error" || usage === "accept" 
                    ? handleClose()
                    : usage === "deleteRecipe"
                    ? action(receivedObject._id)
                    : action()
                }
                }
              >
                aceptar
              </Button>
              {usage === "error" || usage === "accept" ? null : (
                <Button variant="contained" onClick={handleClose}>
                  cancelar
                </Button>
              )}
            </Grid>
          </div>
        </Fade>
      </Modal>
    </>
  );
}
