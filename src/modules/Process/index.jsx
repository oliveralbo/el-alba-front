import React, { useContext, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import services from "../../api/services";
import AddIcon from "@material-ui/icons/Add";
import CloseIcon from "@material-ui/icons/Close";
import Fab from "@material-ui/core/Fab";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";
import FormControl from "@material-ui/core/FormControl";
import { useAppState } from "../../context";
import Functions from "../../utils/gralFunctions";
import ProcessModal from "./components/ProcessModal";
import { stockContext } from "../../context/stockContext";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  button: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    width: "100%",
    marginBottom: "2%",
  },
  select: {
    width: "100%",
    marginTop: "1%",
  },
  input: {
    marginTop: "2%",
  },
  divider: {
    color: "black",
    marginTop: "10%",
    width: "160%",
  },
  Icon: {
    marginTop: "5%",
    marginLeft: "50%",
    width: "12%",
    height: "0%",
  },
  selectWithDelete: {
    display: "flex",
    justifyContent: "space-between",
  },
}));

export default function Process(props) {
  const { setMyModule,setIsClean } = props;
  const { contextMp, setContextMp, contextProd, setContextProd, getStocks } =
    useContext(stockContext);
  const classes = useStyles();
  const history = useHistory();
  const { userContext } = useAppState();
  const [selectedMps, setSelectedMps] = useState([]);
  const [selCantMps, setSelCantMps] = useState([]);
  const [selMpNames, setSelMpNames] = useState([]);
  const [quantityMp, setQuantityMp] = useState([1]);
  const [recipes, setRecipes] = useState(null);
  const [prodSelected, setProdSelected] = useState("");
  const [prodSelectedName, setProdSelectedName] = useState("");
  const [cantProd, setCantProd] = useState(0);
  const [openModal, setOpenModal] = useState(false);
  const [objectData, setObjectData] = useState(null);
  const [usage, setUsage] = useState("");
  const [titleModal, setTitleModal] = useState(null);
  const [message, setMessage] = useState(null);
  const [comeRecipes, setComeRecipes] = useState(false)

  useEffect(() => {
    getAllRecipes();
    // setMyModule("")
  }, []);

  const getAllRecipes = async () => {
    try {
      const dataRecipes = await services.getRecipes();
      setRecipes(dataRecipes.data ? dataRecipes.data.records : dataRecipes);
    } catch (e) {
      console.log(e);
    }
  };

  const handleChangeMp = (event) => {
    let mpsCopy = [...selectedMps];
    mpsCopy[event.target.name] = event.target.value.split("-")[0];
    setSelectedMps(mpsCopy);

    let mpsNamesCopy = [...selMpNames];
    mpsNamesCopy[event.target.name] = event.target.value.split("-")[1];
    setSelMpNames(mpsNamesCopy);
  };

  const handleChangeCantMp = (event) => {
    if (event.target.value >= 0) {
      let cantMpsCopy = [...selCantMps];
      cantMpsCopy[event.target.name] = event.target.value;
      setSelCantMps(cantMpsCopy);
    } else {
      event.target.value = 0;
    }
  };

  const handleChangeProduct = (event) => {
    setProdSelected(event.target.value.split("-")[0]);
    setProdSelectedName(event.target.value.split("-")[1]);
  };

  const handleChangeCantProd = (event) => {
    if (event.target.value >= 0) {
      setCantProd(event.target.value);
    } else {
      event.target.value = 0;
    }
  };

  const addRecipe = async (objectRecipe) => {
    // try {
    //   await services.addRecipes(objectRecipe);
      setOpenModal(false);
      clean();
    // } catch (e) {
    //   setError(e)

    // }
  };

  const handleChangeRecipes = async (event) => {
    let recipe = event.target.value;
    console.log('recipe')
    if (recipe === "") {
      // setRecipeSelected(null);
      setProdSelected("");
      setCantProd(null);
    } else if (recipe === 1) {
      let proceso = takeProcess();
      if (proceso) {
        console.log("proceso: ", proceso);
        let objectRecipe = Functions.processToRecipes(
          proceso,
          contextMp,
          contextProd
        );
        
        try {
          await services.addRecipes(objectRecipe);
          // setObjectData(objectRecipe);
          setUsage("accept");
          setTitleModal("Recetas");
          setMessage(
            `${objectRecipe.product.key} ha sido agregado a las recetas`
          );
          setOpenModal(true);
        } catch (e) {
          setUsage("error");
          setTitleModal("Error");
          setMessage(e.message.split(":")[2]);
          setOpenModal(true);
        }
        
      } else {
        setUsage("error");
        setTitleModal("Error");
        setMessage("Complete los datos de la receta");
        setOpenModal(true);
      }
    } else {
      let objectProcess = Functions.recipesToProcess(recipe,  contextMp, contextProd);
      setSelMpNames(recipe.ingredients.keys)  
      setProdSelectedName(recipe.product.key)
      procesed(null, objectProcess)
      setComeRecipes(true)
      //oli aca toca
    }
  };

  const addSelect = () => {
    setQuantityMp([...quantityMp, 1]);
  };

  const clean = () => {
        let users = {};
        users.user = userContext;
        localStorage.setItem("module", "Producción" );
        history.push("/home", { users: users });// ver de vaciar esto para el refresco en otras pantallas
        window.location = window.location;
        setIsClean(true)
      };

  const validate0 = (process) => {
    let values = Object.values(process.mp);
    values.map((none) => {
      let found = values.findIndex((element) => element === 0);
      if (found != -1) {
        let llaves = Object.keys(process.mp);
        delete process.mp[llaves[found]];
        found = -1;
      }
    });
    return process;
  };

  const takeProcess = () => {
    let proceso = {};
    proceso.mp = {};
    proceso.pro = {};
    proceso.pro[prodSelected] = Number(cantProd);
    if (
      prodSelected &&
      cantProd > 0 &&
      selectedMps.length > 0 &&
      selCantMps.length > 0
    ) {
      for (let i = 0; i < selectedMps.length; i++) {
        if (proceso.mp[selectedMps[i]]) {
          proceso.mp[selectedMps[i]] =
            proceso.mp[selectedMps[i]] + Number(selCantMps[i]);
        } else {
          proceso.mp[selectedMps[i]] = Number(selCantMps[i]);
        }
      }
      let finalProcess = validate0(proceso);
      if (Object.keys(finalProcess.mp).length === 0) {
        return false;
      }
      proceso = finalProcess;
    } else {
      return false;
    }
    return proceso;
  };

  const validateStock = (data) => {
    let pkey = Object.keys(data);
    let pvalues = Object.values(data);
    let insufficientMps = [];
    pkey.map((x, i) => {
      return contextMp.map((oneStock) => {
        if (oneStock._id === x) {
          if (pvalues[i] > oneStock.quantity) {
            insufficientMps.push(oneStock.product);
          }
        }
      });
    });
    return insufficientMps;
  };

  const procesed = (e, recipeData=null) => {

    let data = recipeData ? recipeData : takeProcess();
    console.log(e.target)
    let insufficents;
    if (data) {
      insufficents = validateStock(data.mp);
      if (insufficents.length > 0) {
        setObjectData(insufficents);
        setUsage("error");
        setTitleModal("Proceso");
        setMessage(`No tiene sufieciente <b>${insufficents}</b>`);
        setOpenModal(true);
      } else {
        setObjectData(data);
        setUsage("ok");
        setTitleModal("Proceso");
        setMessage("Proceso realizado con exito");
        setOpenModal(true);
      }
    } else {
      setUsage("error");
      setTitleModal("Error");
      setMessage("Ingrese ingredientes validos por favor");
      setOpenModal(true);
    }
  };

  const handleCloseModal = () => {
    setOpenModal(false);
    if(comeRecipes || usage === "accept"){
      clean()
      setComeRecipes(false)
    }
  };

  const handleProcess = async () => {
    try {
      await services.process(objectData);
      await getStocks();
      setOpenModal(false);
      clean();
    } catch (e) {
      console.log(e);
    }
  };

  const deleteRecipe = async (id) => {
    try {
      await services.deleteRecipe(id)
      setOpenModal(false);
      clean()
    } catch (e) {
      console.error(e);
    }
  };

  const handleDelete = (e, recipe) => {
    e.preventDefault();
    e.stopPropagation();
    setObjectData(recipe);
    setUsage("deleteRecipe");
    setTitleModal("Eliminar receta");
    setMessage(
      `Va a eliminar la receta de ${recipe.product.key}, Esta seguro ?`
    );
    setOpenModal(true);
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={8}>
        <Grid item xs={12}>
          <h1>Proceso de Producto</h1>
        </Grid>

        <Grid item xs={4}>
          <InputLabel id="demo-simple-select-label">
            Seleccione Materia prima
          </InputLabel>
          {quantityMp.map((item, index) => {
            return (
              <Select
                labelId="demo-simple-select-label"
                // id="demo-simple-select"
                onChange={handleChangeMp}
                className={classes.select}
                name={index}
                key={index}
              >
                {contextMp &&
                  contextMp.map((x) => {
                    return (
                      <MenuItem key={x._id} value={`${x._id}-${x.product}`}>
                        {x.product}
                      </MenuItem>
                    );
                  })}
              </Select>
            );
          })}
          <Fab color="primary" className={classes.Icon} onClick={addSelect}>
            <AddIcon />
          </Fab>
          <Divider variant="large" className={classes.divider} />
        </Grid>
        <Grid item xs={2}>
          <InputLabel id="demo-simple-select-label">Cant.</InputLabel>
          {quantityMp.map((item, index) => {
            return (
              <TextField
                className={classes.input}
                id="standard-number"
                type="number"
                onChange={handleChangeCantMp}
                InputLabelProps={{
                  shrink: true,
                }}
                name={index}
              />
            );
          })}
        </Grid>
        <Grid item xs={5}>
          <Button
            variant="outlined"
            className={classes.button}
            onClick={procesed}
          >
            Procesar
          </Button>
          <Button variant="outlined" className={classes.button} onClick={clean}>
            Limpiar
          </Button>
        </Grid>

        {/* /********************************renglon */}

        <Grid item xs={4}>
          <InputLabel id="demo-simple-select-label">Producto</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            onChange={handleChangeProduct}
            className={classes.select}
            // name="popo"
          >
            {contextProd?.map((x) => {
              return (
                <MenuItem value={`${x._id}-${x.product}`}>{x.product}</MenuItem>
              );
            })}
          </Select>
        </Grid>
        <Grid item xs={2}>
          <TextField
            id="standard-number"
            label="Cantidad"
            type="number"
            onChange={handleChangeCantProd}
            value={cantProd}
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>
        <Grid item xs={5}>
          <FormControl variant="outlined" className={classes.select}>
            <InputLabel id="demo-simple-select-outlined-label">
              Recetas
            </InputLabel>
            <Select
              labelId="demo-simple-select-outlined-label"
              id="demo-simple-select-outlined"
              value={recipes}
              onChange={handleChangeRecipes}
              label="Recetas"
            >
              <MenuItem value={""}>
                <em>ninguna</em>
              </MenuItem>
              {recipes &&
                recipes.map((x) => {
                  return (
                    <MenuItem value={x} className={classes.selectWithDelete}>
                      {x.product.key}
                      <CloseIcon
                        onClick={(event) => handleDelete(event, x)}
                        style={{ zIndex: "100" }}
                      />
                    </MenuItem>
                  );
                })}
              <MenuItem value={1}>Agregar a Recetas...</MenuItem>
            </Select>
          </FormControl>
        </Grid>
      </Grid>

      <ProcessModal
        handleOpen={openModal}
        handleClose={handleCloseModal}
        receivedObject={objectData}
        usage={usage}
        title={titleModal}
        mps={selMpNames}
        cantMp={quantityMp}
        prod={prodSelectedName}
        action={
             titleModal === "Eliminar receta"
            ? deleteRecipe
            : handleProcess
        }
        msg={message}
        // selectedItem={objectData} //aca va la data con o sin el error
      />
    </div>
  );
}
