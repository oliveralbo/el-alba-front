import React, { useState, useEffect } from "react";
import jwt_decode from "jwt-decode";
import FormGral from "../../components/FormGral";
import services from "../../api/services";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import ModalGral from "../../components/ModalGral";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Typography from "@material-ui/core/Typography";
import useMediaQuery from "@material-ui/core/useMediaQuery";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
  },
  form: {
    width: "60%",
  },
  fullList: {
    width: "auto",
  },
  select: {
    width: "100%",
    marginTop: "1%",
  },
  msjError: {
    marginTop: "10%",
    color: "red",
    textAlign: "center",
  },
  button: {
    marginTop: "10%",
    backgroundColor: "#ffffff",
    color: "black",
    border: "solid 1px black",
    width: "100%",
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function AddEmployed() {
  const isSmall = useMediaQuery("(max-width:600px)");
  const classes = useStyles();
  const [message, setMessage] = useState(null);
  const [usage, setUsage] = useState("");
  const [titleModal, setTitleModal] = useState(null);
  const [selectedItem, setSelectedItem] = useState(null);
  const [openModal, setOpenModal] = useState(false);
  const [allUsers, setAllUsers] = useState([]);
  const [user, setUser] = useState(null);
  const [borderError, setBorderError] = useState("");
  const [error, setError] = useState(false);
  const [msjError, setMsjError] = useState("");
  const [refresh, setRefresh] = useState(0);
  const [objectValues, setObjectValues] = useState(null);

  const withCredentials = [
    {
      component: "field",
      type: "password",
      required: true,
      name: "password",
      placeholder: "Contraseña",
      style: {
        width: "85%",
        marginRight: "8%",
        borderColor: borderError,
      },
    },
    {
      component: "field",
      type: "password",
      required: true,
      name: "validatePassword",
      placeholder: "Repetir contraseña",
      style: {
        width: "85%",
        marginRight: "8%",
        borderColor: borderError,
      },
    },
  ];

  const withoutCredentials = [
    {
      component: "field",
      type: "text",
      required: true,
      name: "name",
      placeholder: "Nombre/s",
      style: {
        width: "85%",
        marginRight: "8%",
      },
    },
    {
      component: "field",
      type: "text",
      required: true,
      name: "surname",
      placeholder: "Apellido",
      style: {
        width: "85%",
        marginRight: "8%",
      },
    },
    {
      component: "field",
      type: "text",
      required: true,
      name: "dni",
      placeholder: "DNI",
      style: {
        width: "85%",
        marginRight: "8%",
      },
    },
    {
      component: "field",
      type: "text",
      required: true,
      name: "phone",
      placeholder: "Teléfono",
      style: {
        width: "85%",
        marginRight: "8%",
      },
    },
    {
      component: "field",
      type: "email",
      required: true,
      name: "email",
      placeholder: "E-mail",
      style: {
        width: "85%",
        marginRight: "8%",
      },
    },
    {
      component: "checkbox",
      type: "checkbox",
      name: "role",
      label: "Administrador",
    },
    {
      component: "button",
      type: "submit",
      fullWidth: true,
      variant: "contained",
      color: "primary",
      label: "Enviar",
      style: {
        backgroundColor: "#ffffff",
        color: "black",
        border: "solid 1px black",
        width: "50%",
      },
    },
  ];

  const [assemble, setAssemble] = useState(withoutCredentials);
  useEffect(() => {
    getAllUsers();
  }, []);
  useEffect(() => {
    if(assemble.length === 9){
      return false
    }else{
      let tem = [...withoutCredentials]
      tem.splice(5, 0, ...withCredentials)
      !selectedItem && setAssemble(tem)
    }
  }, [selectedItem,error]);

  const handleSelect = (userTarget) => {
    setSelectedItem(userTarget._id);
    setUser(userTarget);
    setAssemble(withoutCredentials);
  };

  /**************************************************v */

  const onHandleDelete = () => {
    setMessage(
      `Está seguro que desea eliminar al usuario ${
        user.name + " " + user.surname
      }?`
    );
    setTitleModal("Borrar");
    setUsage("delete");
    setOpenModal(true);
  };

  const onHandleAdd = (values) => {
    try {
      setMessage(`Está a punto de agregar un nuevo usuario/empleado.`);
      setTitleModal("Agregar");
      setUsage("add");
      setObjectValues(values);
      setOpenModal(true);
    } catch (e) {
      console.log(e);
    }
  };

  const onHandleUpdate = (values) => {
    try {
      setMessage(
        `Va a modificar los datos del usuario ${
          user.name + " " + user.surname
        }?`
      );
      setTitleModal("Modificar");
      setUsage("update");
      setObjectValues(values);
      setOpenModal(true);
    } catch (e) {
      console.log(e);
    }
  };

  const onHandlePass = () => {
    let tem = [...withoutCredentials];
    tem.splice(5, 0, ...withCredentials);
    setAssemble(assemble.length < 9 ? tem : withoutCredentials);
  };

  const handleRefresh = () => {
    setRefresh(refresh + 1);
    getAllUsers();
    setUser(null);
    setSelectedItem(null);
  };

  const onSubmit = async (values) => {
    values.role === true ? (values.role = "admin") : (values.role = "employee");
    if (values.validatePassword !== values.password) {
      setMsjError("Usuario y contraseña deben coincidir");
      setError(true);
      setBorderError("red");
      setTimeout(() => {
        setBorderError("#ccc");
        setError(false);
      }, 5000);
      return false;
    } else {
      if (values.validatePassword) {
        delete values.validatePassword;
      }

      if (selectedItem) {
        onHandleUpdate(values);
      } else {
        onHandleAdd(values);
      }
    }
  };

  const onDelete = async () => {
    console.log("se ejecuto !!!");
    try {
      await services.deleteUser(selectedItem);
      setOpenModal(false);
      handleRefresh();
    } catch (e) {
      setTitleModal("Error");
      setMessage("Algo ha salido mal, vuelva a intentarlo mas tarde");
      setUsage("error");
      setOpenModal(true);
      console.log(e);
    }
  };

  const onAdd = async (values) => {
    try {
      await services.addUsers(values);
      setOpenModal(false);
      handleRefresh();
    } catch (e) {
      setTitleModal("Error");
      setMessage("Algo ha salido mal, vuelva a intentarlo mas tarde");
      setUsage("error");
      setOpenModal(true);
      console.log(e);
    }
  };

  const onUpdate = async (values) => {
    try {
      await services.updateUser(selectedItem, values);
      setOpenModal(false);
      handleRefresh();
    } catch (e) {
      setTitleModal("Error");
      setMessage("Algo ha salido mal, vuelva a intentarlo mas tarde");
      setUsage("error");
      setOpenModal(true);
      console.log(e);
    }
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const getAllUsers = async () => {
    try {
      const data = await services.getUsers();
      const decoded = jwt_decode(localStorage.getItem("usrToken")).user._id;
      let users = [];
      data.records.map((user) => {
        if (user._id !== decoded) {
          users.push(user);
        }
      });
      setAllUsers(users);
    } catch (e) {
      console.log(e); //ver q hacer con este error
    }
  };

  return (
    <Grid item xs={12}>
      <h1>Empleados</h1>

      <div className={!isSmall ? classes.container : null}>
        <div className={classes.form}>
          <FormGral
            assemble={assemble}
            onSubmit={onSubmit}
            selectedItem={user}
          />
        </div>
        <div style={{ display: "flex", flexDirection: "column" }}>
          <Typography variant="h6" gutterBottom>
            Seleccione empleado a eliminar o borrar
          </Typography>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            onChange={(e) => handleSelect(e.target.value)}
            className={classes.select}
            name="popo"
          >
            <MenuItem value={""}>
              <em>ninguno</em>
            </MenuItem>
            {allUsers?.map((x) => {
              return (
                <MenuItem value={x}>
                  {x.name} {x.surname}
                </MenuItem>
              );
            })}
          </Select>
          {selectedItem ? (
            <>
              <Button className={classes.button} onClick={onHandleDelete}>
                Eliminar
              </Button>
              <Button className={classes.button} onClick={onHandlePass}>
                {assemble.length === 7
                  ? `Cambiar contraseña`
                  : `No cambiar contraseña`}
              </Button>
            </>
          ) : null}

          {error ? <p className={classes.msjError}>{msjError}</p> : null}
        </div>
      </div>

      <ModalGral
        open={openModal}
        handleClose={handleCloseModal}
        message={message}
        title={titleModal}
        objectValues={objectValues}
        action={
          usage === "delete"
            ? onDelete
            : usage === "add"
            ? onAdd
            : usage === "update"
            ? onUpdate
            : null
        }
        selectedItem={selectedItem}
        refresh={handleRefresh}
      />
    </Grid>
  );
}
