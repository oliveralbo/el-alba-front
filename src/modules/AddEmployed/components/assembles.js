const withCredentials = [
  {
    component: "field",
    type: "password",
    required: true,
    name: "password",
    placeholder: "Contraseña",
    style: {
      width: "85%",
      marginRight: "8%",
    },
  },
  {
    component: "field",
    type: "password",
    required: true,
    name: "validatePassword",
    placeholder: "Repetir contraseña",
    style: {
      width: "85%",
      marginRight: "8%",
    },
  },
];

const withoutCredentials = [
  {
    component: "field",
    type: "text",
    required: true,
    name: "name",
    placeholder: "Nombre/s",
    style: {
      width: "85%",
      marginRight: "8%",
    },
  },
  {
    component: "field",
    type: "text",
    required: true,
    name: "surname",
    placeholder: "Apellido",
    style: {
      width: "85%",
      marginRight: "8%",
    },
  },
  {
    component: "field",
    type: "text",
    required: true,
    name: "dni",
    placeholder: "DNI",
    style: {
      width: "85%",
      marginRight: "8%",
    },
  },
  {
    component: "field",
    type: "text",
    required: true,
    name: "phone",
    placeholder: "Teléfono",
    style: {
      width: "85%",
      marginRight: "8%",
    },
  },
  {
    component: "field",
    type: "email",
    required: true,
    name: "email",
    placeholder: "E-mail",
    style: {
      width: "85%",
      marginRight: "8%",
    },
  },
  {
    component: "checkbox",
    type: "checkbox",
    name: "role",
    label: "Administrador",
  },
  {
    component: "button",
    type: "submit",
    fullWidth: true,
    variant: "contained",
    color: "primary",
    label: "Guardar",
    style: {
      backgroundColor: "#ffffff",
      color: "black",
      border: "solid 1px black",
      width: "50%",
    },
  },
];

export  {withoutCredentials, withCredentials}