const stockMP = [
  {
    _id: 4324523420,
    provider: "Lacteos Tito",
    product: "Masa xyz",
    quantity: 10,
    price: 850.0,
  },
  {
    _id: 4324555523421,
    provider: "Lo de Juancho",
    product: "Masa bbb",
    quantity: 7,
    price: 700.0,
  },
  {
    _id: 4324522,
    provider: "Sal si puedes",
    product: "Sal xxx",
    quantity: 9.2,
    price: 877.5,
  },
  {
    _id: 4324523423,
    provider: "Acei Tito",
    product: "Aceite de pepe",
    quantity: 10,
    price: 850.23,
  },
];

export default stockMP;
