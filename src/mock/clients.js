const clients = [
  {
    _id:578,
    name: "Diego Perez",
    company: "Guimpi",
    phone: 45684546,
    address: "E. Lamarca 2021",
    location: "caba",
    email: "guimpi@supizza.com.ar",
    account:  `$ ${7500}`,
    orders : [{
      day : "13/2/18",
      amount: 350,
      product: "Muzza A",
      quantity: 10
    },{
      day : "13/2/18",
      amount: 350,
      product: "Muzza A",
      quantity: 10
    },{
      day : "13/2/18",
      amount: 350,
      product: "Muzza A",
      quantity: 10
    },{
      day : "13/2/18",
      amount: 350,
      product: "Muzza A",
      quantity: 10
    }],
    payments: [
      {
        day : "13/2/18",
        amount: 350,
        comments: ""
      }
    ]

  },
  {
    _id:57998,
    name: "Juan Garcia",
    company: "lo de Dany",
    phone: 45667066,
    address: "B. Blanca 1999",
    location: "3 de febrero",
    email: "soyPro@Globoludo.com.ar",
    account: `$ ${300}`,
  }
];

export default clients;
