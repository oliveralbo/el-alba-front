const users = [
  {
    isAdmin: false,
    _id: "5e1138965d1b3b07ccc2631c",
    name: "pepe",
    surname: "pepe",
    email: "pepe@pepe.com",
    __v: 0
  },
  {
    isAdmin: false,
    _id: "5e11397bd4fc2b3604ae8b21",
    name: "pablo",
    surname: "pablo",
    email: "pablo@pablo.com",
    __v: 0
  }
];

export default users;
