const stockProd = [
  {
    _id: 10004523425,
    type: "A",
    product: "Muzza A",
    quantity: 10,
    price: 850.0
  },
  {
    _id: 10004523427,
    type: "B",
    product: "Muzza B",
    quantity: 4,
    price: 900.0
  },
  {
    _id: 10004523429,
    type: "C",
    product: "Muzza C",
    quantity: 10,
    price: 1850.0
  },
  {
    _id: 10004523423,
    type: "D",
    product: "Muzza D",
    quantity: 70,
    price: 300.0
  }
];

export default stockProd;
